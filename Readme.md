# gwInstall

## What

A simple bash script for Fedora SE Linux that will:

1. Copy the geneweb distribution folder (the result of `make distrib`) to a location outside of the build folder.
1. Create the geneweb base folder, where the users database will reside. Note: this should not be under the distribution directory or it may be overwritten on update.
1. Create the geneweb user, if necessary.
1. Add geneweb to systemd (/etc/sysconfig/geneweb and /etc/systemd/system/geneweb.service).
1. Set the appropriate SE context on the geneweb daemon.
1. Give some hints on what comes next.

This script isn't meant to be very robust - it was written and tested only for my limited use case. I am publishing it anyways as someone may find parts of it useful.

## Why

There was no prebuilt geneweb package for Fedora on aarch64 so I had to build it myself. This script just simplifies the installation. 

## How

* Follow the instructions on [Github](https://github.com/geneweb/geneweb) to build geneweb. 
* **Don't forget to `git checkout distrib-6-08-ocaml-4-xx` after `git clone` if you want the latest stable release!**
* Run this script, passing it the distribution folder location, and follow the prompts.

